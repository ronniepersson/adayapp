//
//  ADAYCatalogueWS.h
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//
#import <CoreData/CoreData.h>
#import "AdayWSClient.h"
#import "Map.h"
#import "Catalogue.h"
#import "ADAYAppDelegate.h"

@interface ADAYCatalogueWS : AdayWSClient<NSXMLParserDelegate>
{
    id <NSXMLParserDelegate> delegate;
@private NSString* currentSubPart;
}

@property (nonatomic,retain) UIViewController *finishedDelegate;
@property SEL finishedSelector;

@property (nonatomic,retain ) Catalogue *currentCatalogue;
//-(void) getCatalogesForMap: ( Map *) map;
-(void) getCatalogesForProject: ( Map *) map; // Replaces getCatalogesForMap

- (NSDate*)parseRFC3339DateTimeString:(NSString *)rfc3339DateTimeString;
- (NSNumber*) parseStringToNumber:(NSString*)string;

-(void) finishedWithData: (NSData *) data;

@end
