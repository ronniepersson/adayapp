//
//  SignInViewController.h
//  ADay
//
//  Created by Ronnie Persson on 2011-12-22.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADAYMapWS.h"
#import "ADAYCatalogueWS.h"
#import "ADAYCodeWS.h"

@interface SignInViewController : UIViewController<UITextFieldDelegate>
{
    int nrOfCataloges;
}
@property ( nonatomic, retain ) ADAYMapWS *mapWS;
@property ( nonatomic, retain ) ADAYCatalogueWS *catalogueWS;
@property ( nonatomic, retain ) ADAYCodeWS *codeWS;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activity;
- (IBAction)signIn:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *signIn;
@property (retain, nonatomic) IBOutlet UIButton *signInButton;
@property (retain, nonatomic) IBOutlet UIButton *demoButton;
- (IBAction)demoSignIn:(id)sender;

@end
