//
//  TextButton.m
//  ADay
//
//  Created by Ronnie Persson on 2011-12-21.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "TextButton.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreData/CoreData.h>
#import "ADAYAppDelegate.h"

@implementation TextButton
@synthesize delegateController;
@synthesize code;
@synthesize rectsToDraw;
@synthesize textField;

-(id) initWithTextField: (UITextView *) textfield andCode: ( Code *) aCode
{
    self = [ super init ];
    if ( self )
    {
        
        self.code = aCode;
        self.textField = textfield; 
        
        [[NSNotificationCenter defaultCenter] 
         addObserver:self 
         selector:@selector(textUpdated:)
         name:UITextViewTextDidChangeNotification 
         object:nil];
        
        [[NSNotificationCenter defaultCenter] 
         addObserver:self 
         selector:@selector(textUpdated:)
         name: @"ADAYWordWasReplaced" 
         object: nil ];
        
        
        
        
        self.userInteractionEnabled = NO;
        self.rectsToDraw = nil;
        
        
    }
    return  self;
}

-(UIColor*) colorForType
{
    if ( [ self.code.catalogue.type isEqualToString: @"Activity" ] )
    {
        return [ UIColor colorWithRed: 253.0 / 255.0 green: 122.0 / 255.0 blue: 122.0 / 255.0 alpha: 1.0 ];
    }
    
    if ( [ self.code.catalogue.type isEqualToString: @"Location" ] )
    {
        return [ UIColor colorWithRed: 112.0 / 255.0 green: 112.0 / 255.0 blue: 252.0 / 255.0 alpha: 1.0 ];
    }
    
    //With who
    return [ UIColor colorWithRed: 113.0 / 255.0 green: 249.0 / 255.0 blue: 113.0 / 255.0 alpha: 1.0 ];
    
}

-(void) textUpdated: ( NSNotification *) notification
{
    
    
    
    NSRange range = [ self.textField.text rangeOfString: self.code.text ];
    
    if ( range.length == 0 ) //dead object
    {
        self.hidden = YES;
        [ self setNeedsDisplay ];
        [[ NSNotificationCenter defaultCenter ] postNotificationName: @"ADAYTextButtonDead" object:nil ];
        return;
    }
    
    self.hidden = NO;
    
    
    UITextPosition *start = [ self.textField closestPositionToPoint: CGPointMake( 0, 0 ) ];
    UITextPosition *startWord = [ self.textField positionFromPosition: start offset: range.location ];
    UITextPosition *endWord = [ self.textField positionFromPosition: startWord offset: range.length ];
    
    UITextRange *textRange = [ textField textRangeFromPosition: startWord toPosition: endWord ];
    CGRect r = [ textField firstRectForRange: textRange ];
    self.frame = textField.frame;
    CGRect firstRect = CGRectMake( r.origin.x, r.origin.y + r.size.height - 3, r.size.width, 3 ); 
    self.rectsToDraw = [ NSMutableArray arrayWithObject: [ NSValue valueWithCGRect: firstRect ] ];
        
    //Two or more words that can be on two or more lines
    if ( [[ self.code.text componentsSeparatedByString: @" " ] count ] > 1 )
    {
        NSArray *wordsInWord = [ self.code.text componentsSeparatedByString: @" " ];
        
        
        int n = 0;
        for ( NSString *word in wordsInWord )
        {
            if ( n > 0 )
            {
                NSRange secondaryRange = [ self.textField.text rangeOfString: word options:NSCaseInsensitiveSearch range: range ];
                
                if ( secondaryRange.location <= range.location )
                    break;
                    
                UITextPosition *startSecond = [ self.textField closestPositionToPoint: CGPointMake( 0, 0 ) ];
                UITextPosition *startWordSecond = [ self.textField positionFromPosition: startSecond offset: secondaryRange.location ];
                UITextPosition *endWordSecond = [ self.textField positionFromPosition: startWordSecond offset: secondaryRange.length ];
                UITextRange *secondaryTextRange = [ textField textRangeFromPosition: startWordSecond toPosition: endWordSecond ];
                
                CGRect r2 = [ textField firstRectForRange: secondaryTextRange ];
                if ( r2.origin.y != r.origin.y )
                {
                    CGRect secondaryRect = CGRectMake( r2.origin.x, r2.origin.y + r2.size.height - 3, r2.size.width, 3 );
                    [ self.rectsToDraw addObject: [ NSValue valueWithCGRect: secondaryRect ] ];
                }
            }
            n++;
        }
        
        
    }
        

    self.backgroundColor = [ UIColor clearColor ];
      
    [ self setNeedsDisplay ];
    
}


-(void) drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor( context, [ self colorForType ].CGColor );
   // CGContextSetAlpha(context, 0.5);
    
    if ( rectsToDraw != nil )
    {
        for ( NSValue *r in self.rectsToDraw )
        {
            CGRect rect = [ r CGRectValue ];
            CGRect offsetted = CGRectMake( rect.origin.x - self.textField.contentOffset.x , rect.origin.y - self.textField.contentOffset.y, rect.size.width, rect.size.height );
            CGContextFillRect( context, offsetted );
        }
    }
   
}



-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self ];
    [ delegateController release ];
    [ super dealloc ];
}


@end
