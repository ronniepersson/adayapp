//
//  ADAYMapWS.h
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "AdayWSClient.h"
#import "Map.h"
#import "ADAYAppDelegate.h"

@interface ADAYMapWS : AdayWSClient<NSXMLParserDelegate>
{
    id <NSXMLParserDelegate> delegate;
    @private NSString* currentSubPart;
}

@property (nonatomic,retain) UIViewController *finishedDelegate;
@property SEL finishedSelector;

@property (nonatomic,retain ) Map *currentMap;
//-(void)initMapWithName: (NSString *)name;

- (NSDate*)parseRFC3339DateTimeString:(NSString *)rfc3339DateTimeString;
- (NSNumber*) parseStringToNumber:(NSString*)string;


-(void) setMapWithName: (NSString *)name;

@end
