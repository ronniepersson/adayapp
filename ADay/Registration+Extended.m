//
//  Registration.m
//  Aday
//
//  Created by Stefan Norberg on 2011-11-07.
//  Copyright (c) 2011 Medc AB. All rights reserved.
//

#import "Registration+Extended.h"
#import "Map.h"
#import "TextButton.h"
#import "ADAYAppDelegate.h"
@implementation Registration(Extended)


const NSString* namespace = @"http://schemas.datacontract.org/2004/07/AdayWS";

- (NSString*) toXmlWithGuid: (NSString *) guid {
    NSMutableString *myString = [NSMutableString stringWithString:@"<Registration>"];
    [myString appendFormat:@"<Id>%@</Id>", guid ];
    [myString appendFormat:@"<MapId>%@</MapId>",self.map.mapId ];
    [myString appendFormat:@"<TimeStamp>%@</TimeStamp>", [ self dateToXmlString: self.timeStamp ] ];
    [myString appendFormat:@"<DurationInMinutes>%@</DurationInMinutes>", self.durationInMinutes ];
    [myString appendFormat:@"<ActivityCode1>%@</ActivityCode1>", self.activityCode1];
    [myString appendFormat:@"<ActivityCode2>%@</ActivityCode2>", self.activityCode2 != nil ? self.activityCode2 : @"" ];
    [myString appendFormat:@"<LocationCode>%@</LocationCode>", self.locationCode];
    [myString appendFormat:@"<TogetherCode1>%@</TogetherCode1>", self.togetherCode1];
    [myString appendFormat:@"<TogetherCode2>%@</TogetherCode2>", self.togetherCode2 != nil ? self.togetherCode2 : @"" ];
    [myString appendFormat:@"<TogetherCode3>%@</TogetherCode3>", self.togetherCode3 != nil ? self.togetherCode3 : @"" ];
    [myString appendFormat:@"<MoodCode>%@</MoodCode>", self.moodCode != nil ? self.moodCode : [ NSNumber numberWithInt: 0 ] ];
    
    [myString appendString:@"</Registration>"];
    return myString;
}

- (NSString*) dateToXmlString: ( NSDate *) date {
    // Format the dates rfc3339DateTimeString
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    return [dateFormatter stringFromDate: date];
}

+(Registration *) insertRegistrationWithMap: ( Map *) map
{
    ADAYAppDelegate *appDelegate = ( ADAYAppDelegate * ) [ [ UIApplication sharedApplication ] delegate ];
   
    Registration *r = [[ Registration alloc ] initWithEntity:
                       [ NSEntityDescription entityForName: @"Registration"
                                    inManagedObjectContext:appDelegate.managedObjectContext
                        ]
                              insertIntoManagedObjectContext: appDelegate.managedObjectContext
                       ];
    
        r.map = map;
        
        NSError *error;
        [ appDelegate.managedObjectContext save: &error ];
    
    return r;
}


@end
