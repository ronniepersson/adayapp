//
//  main.m
//  ADay
//
//  Created by Ronnie Persson on 2011-11-21.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ADAYAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ADAYAppDelegate class]));
    }
}
