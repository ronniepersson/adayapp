//
//  Map.m
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "Map.h"


@implementation Map

@dynamic endDate;
@dynamic mapId;
@dynamic language;
@dynamic name;
@dynamic projectid;
@dynamic startDate;
@dynamic descriptionText;

@end
