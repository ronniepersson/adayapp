//
//  InfoViewController.h
//  ADay
//
//  Created by Ronnie Persson on 2012-04-20.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIWebView *infoWebView;

@end
