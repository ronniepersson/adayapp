//
//  ADAYCodeWS.h
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "AdayWSClient.h"
#import "Catalogue.h"
#import "Code.h"
#import "Map.h"
#import "ADAYAppDelegate.h"

@interface ADAYCodeWS : AdayWSClient<NSXMLParserDelegate>
{
    id <NSXMLParserDelegate> delegate;
@private NSString* currentSubPart;
}
@property ( nonatomic, retain ) Catalogue *catalogue;
@property ( nonatomic, retain ) NSMutableSet *codes;
@property (nonatomic,retain) UIViewController *finishedDelegate;
@property SEL finishedSelector;
@property SEL finishedAllSelector;
@property (nonatomic,retain ) NSString *currentString;

@property (nonatomic,retain ) Code *currentCode;
//-(void) getCodesForCatalogue: ( Catalogue *) catalogue Map: ( Map *) map;
-(void) getCodesInCatalogueForProject: ( Catalogue *) cat Map: ( Map *) map; // Replaces getCodesForCatalogue

- (NSDate*)parseRFC3339DateTimeString:(NSString *)rfc3339DateTimeString;
- (NSNumber*) parseStringToNumber:(NSString*)string;

-(void) finishedWithData: (NSData *) data;

@end