//
//  ChooseTimeViewController.m
//  ADay
//
//  Created by Ronnie Persson on 2012-04-20.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "ChooseTimeViewController.h"
#import "ADAYAppDelegate.h"

@interface ChooseTimeViewController ()

@end

@implementation ChooseTimeViewController
@synthesize timePicker;
@synthesize registration;
@synthesize timeLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString( @"When?", @"Title" );
    
    [ self.timePicker setDate: [ NSDate date ] ];
    [ self timeChanged: nil ];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setTimePicker:nil];
    [self setTimeLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [timePicker release];
    [timeLabel release];
    [super dealloc];
}

#pragma mark - Actions

- (IBAction)timeChanged:(id)sender {
    NSDate *date = self.timePicker.date;
    NSDateFormatter *formatter = [ [ NSDateFormatter alloc ] init ];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    timeLabel.text = [ formatter stringFromDate: date ];
    [ formatter release ];
}

- (IBAction)done:(id)sender {
    self.registration.timeStamp = self.timePicker.date;
    
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    NSError *error;
    [ appDelegate.managedObjectContext save: &error ];
    
    
    [ [ NSNotificationCenter defaultCenter ] postNotificationName: @"ADAYNeedSyncingOfRegistrations" object: nil ];
    [ [ NSNotificationCenter defaultCenter ] postNotificationName: @"ADAYCodeIsSubmitted" object: nil ];
    
    UIAlertView *alertView = [ [ UIAlertView alloc ] initWithTitle: @"Aday" message: NSLocalizedString( @"Ready for your next note", @"Alert" ) delegate: nil cancelButtonTitle: nil otherButtonTitles:@"OK", nil ];
    [ alertView show ];
    [ alertView release ];
    
    [ self.navigationController popViewControllerAnimated: YES ];
}




@end
