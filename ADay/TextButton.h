//
//  TextButton.h
//  ADay
//
//  Created by Ronnie Persson on 2011-12-21.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Code.h"

@interface TextButton : UIView
{
}
@property ( nonatomic, retain ) UIViewController *delegateController;
@property ( nonatomic, retain ) Code *code;
@property ( nonatomic, retain ) NSMutableArray *rectsToDraw;
@property ( nonatomic, retain ) UITextView *textField;

-(id) initWithTextField: (UITextView *) textfield andCode: ( Code *) aCode;
-(void) textUpdated: ( NSNotification *) notification;
@end
