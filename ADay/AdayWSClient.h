//
//  AdayWSClient.h
//  Aday
//
//  Created by Stefan Norberg on 2011-11-07.
//  Copyright (c) 2011 Medc AB. All rights reserved.
//
#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

@interface AdayWSClient : NSObject <NSURLConnectionDataDelegate > {

    int action;
}

extern NSString *const WEBSERVICE_ENDPOINT;

@property ( nonatomic, retain ) NSURLConnection *conn;
@property ( nonatomic, retain ) NSMutableData *data;

-(NSString*) getAuthorizationHeader:(NSString*) username password:(NSString*) password;
-(void) finishedWithData: (NSData *) data;

@end
