//
//  ADAYCodeWS.m
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "ADAYAppDelegate.h"
#import "ADAYInsertRegistrationsWS.h"
#import "Map.h"
#import "Registration.h"
#import "Registration+Extended.h"

@implementation ADAYInsertRegistrationsWS
@synthesize currentCode,finishedDelegate,finishedSelector,finishedAllSelector, catalogue,codes;
@synthesize allregistrations;




-(void) insertRegistrationsForMap: ( Map *) map
{
    self.catalogue = catalogue;
    self.codes = [ catalogue mutableSetValueForKey: @"codes" ];
    
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    
    NSURL* url = [[NSURL alloc] initWithString: [ NSString stringWithFormat: @"%@/InsertRegistrations", WEBSERVICE_ENDPOINT]];
    
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[request addValue: [self getAuthorizationHeader:@"ronnie" password:@"123"] forHTTPHeaderField:@"Authorization"];
    
    
    NSString *xml = @"<ArrayOfRegistration xmlns=\"http://schemas.datacontract.org/2004/07/AdayWS\">";
    
  
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"Registration"
                                   inManagedObjectContext: appDelegate.managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    
    [fetchRequest setEntity:entity];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                         initWithKey:@"timeStamp" ascending: YES ];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    
    NSError *error = nil;
    NSArray *registrations =  [ appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error ] ;
   
    for ( Registration *registration in registrations )
    {
        
        xml = [ NSString stringWithFormat: @"%@%@", xml, [ registration toXmlWithGuid: [ ADAYAppDelegate GetUUID ] ] ];
    }
    
    xml = [ NSString stringWithFormat: @"%@</ArrayOfRegistration>", xml ];
    
    self.allregistrations = registrations;
    
    NSData *data = [ xml dataUsingEncoding: NSUTF8StringEncoding];
    
    request.HTTPBody = data;
    
    
    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:[ self retain ] startImmediately: NO ];
    
    self.conn = connection;
    [ connection release ];
    
    self.data = [[ NSMutableData alloc ] init ];
	
    [ self.conn start ];
	[ request release ];
    [ url release ];
    
}

//
// Make sure to set the Registrations property before callinng saveRegistrations!/SN
//
/*
- (void) saveRegistrations:(NSMutableArray*) registrations {
    
 	NSURL* url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@/SaveRegistrations",ServiceEndpoint]];
    
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSData *data = [[self registrationsToXml:registrations] dataUsingEncoding: NSUTF8StringEncoding];
    
    [request setHTTPBody:data];	
	
	NSError *error;
	NSURLResponse *response;
	
	NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	// Log the result
	NSLog(@"%@",[[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding] );
	
	// Load the xml answer, In the case of Saving registrations we just need an ok back so please change this according to the webservice interface/SN
	NSXMLParser* parser = [[NSXMLParser alloc] initWithData:result];
	[parser setDelegate:(id)self];
    [parser setShouldResolveExternalEntities:YES];
    if (![parser parse]) {
		NSLog(@"Could not parse url %@", [url path]);
	} else {
		NSLog(@"Parsed url %@", [url path]);
	}
}*/


-(void) finishedWithData:(NSData *)data
{
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    
   
        for ( Registration *r in allregistrations )
        {
            [ appDelegate.managedObjectContext deleteObject: r ];
        }
        
        NSError *error;
        [ appDelegate.managedObjectContext save: &error ];
        
        self.allregistrations = nil;
    
    
    // Load the xml answer
   /* NSXMLParser* parser = [[NSXMLParser alloc] initWithData:data];
    [parser setDelegate:(id)self];
    [parser setShouldResolveExternalEntities:YES];
    [ parser parse ];
    
    if ( finishedDelegate != nil && finishedAllSelector != nil )
        [ finishedDelegate performSelector: finishedAllSelector ];*/
	
}

#pragma mark - XMLParser delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
	
	//NSLog(@"element Name: %@, ", elementName);
    
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    NSEntityDescription *entity;
    
	
    if ( [elementName isEqualToString:@"Code"] ) 
    {
        entity = [ NSEntityDescription entityForName: @"Code" inManagedObjectContext: appDelegate.managedObjectContext ];
        
        self.currentCode = [[ Code alloc ] initWithEntity: entity insertIntoManagedObjectContext: appDelegate.managedObjectContext ];
        return;
    }
    else {
		currentSubPart = elementName;
	}
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    
	if ( [elementName isEqualToString:@"Code"] ) {
        NSError *error;
        [ codes addObject: currentCode ];
        [ appDelegate.managedObjectContext save: &error ];
        
		if ( finishedDelegate != nil )
            [ finishedDelegate performSelector: finishedSelector withObject: currentCode ];
		currentSubPart = nil;
		return;
    }
    else {
		currentSubPart = nil;
	}
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    
    if ( [currentSubPart isEqualToString:@"Id"] ) {
        currentCode.codeId = string;
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"CodeCatalogId"] ) {
        
        currentCode.catalogueid = string;
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"Lang"]) {
        if ( currentCode.language == nil )
            currentCode.language = string;
        else
            currentCode.language  = [ NSString stringWithFormat: @"%@%@", currentCode.language, string ];
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"Text"]) {
        if ( currentCode.text == nil )
            currentCode.text = string;
        else
            currentCode.text  = [ NSString stringWithFormat: @"%@%@", currentCode.text, string ];
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"Tags"]) {
        if ( currentCode.tags == nil )
            currentCode.tags = string;
        else
            currentCode.tags  = [ NSString stringWithFormat: @"%@%@", currentCode.tags, string ];
        return;
    }
    
}

- (NSDate*) parseRFC3339DateTimeString:(NSString *)rfc3339DateTimeString
// Returns a user-visible date time string that corresponds to the
// specified RFC 3339 date time string. Note that this does not handle
// all possible RFC 3339 date time strings, just one of the most common
// styles.
{
    NSString *          userVisibleDateTimeString;
    NSDateFormatter *   rfc3339DateFormatter;
    NSLocale *          enUSPOSIXLocale;
    
    userVisibleDateTimeString = nil;
    
    // Convert the RFC 3339 date time string to an NSDate.
    
    rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    
    enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    return [rfc3339DateFormatter dateFromString:rfc3339DateTimeString];
}

- (NSNumber*) parseStringToNumber:(NSString*)string {
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    return [f numberFromString:string];
}

@end
