//
//  ADAYCatalogueWS.m
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "ADAYCatalogueWS.h"
#import "Map.h"

@implementation ADAYCatalogueWS
@synthesize currentCatalogue,finishedDelegate,finishedSelector;



//-(void) getCatalogesForMap: ( Map *) map
//{
//    NSURL* url = [[NSURL alloc] initWithString: [ NSString stringWithFormat: @"%@/GetCatalogues?projectid=%@&mapid=%@", WEBSERVICE_ENDPOINT, map.projectid, map.mapId ] ];
//    
//	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
//	[request addValue: [self getAuthorizationHeader:@"ronnie" password:@"123"] forHTTPHeaderField:@"Authorization"];
//    
//    
//    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:[ self retain ] startImmediately: NO ];
//    
//    self.conn = connection;
//    [ connection release ];
//    
//    self.data = [[ NSMutableData alloc ] init ];
//	
//    [ self.conn start ];
//	[ request release ];
//    [ url release ];
//    
//}

-(void) getCatalogesForProject: ( Map *) map
{
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *langCode = [ languages objectAtIndex: 0 ];

    NSURL* url = [[NSURL alloc] initWithString: [ NSString stringWithFormat: @"%@/GetCataloguesForProject?projectid=%@&mapid=%@&lang=%@", WEBSERVICE_ENDPOINT, map.projectid, map.mapId, langCode]];
    
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
	[request addValue: [self getAuthorizationHeader:@"ronnie" password:@"123"] forHTTPHeaderField:@"Authorization"];
    
    
    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:[ self retain ] startImmediately: NO ];
    
    self.conn = connection;
    [ connection release ];
    
    self.data = [[ NSMutableData alloc ] init ];
	
    [ self.conn start ];
	[ request release ];
    [ url release ];
    
}


-(void) finishedWithData:(NSData *)data
{
    // Load the xml answer
    NSXMLParser* parser = [[NSXMLParser alloc] initWithData:data];
    [parser setDelegate:(id)self];
    [parser setShouldResolveExternalEntities:YES];
    [ parser parse ];
}

#pragma mark - XMLParser delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
	
	//NSLog(@"element Name: %@, ", elementName);
    
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    NSEntityDescription *entity;
    
	
    if ( [elementName isEqualToString:@"CodeCatalogue"] ) 
    {
        entity = [ NSEntityDescription entityForName: @"Catalogue" inManagedObjectContext: appDelegate.managedObjectContext ];
        
        self.currentCatalogue = [[ Catalogue alloc ] initWithEntity: entity insertIntoManagedObjectContext: appDelegate.managedObjectContext ];
        return;
    }
    else {
		currentSubPart = elementName;
	}
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    
	if ( [elementName isEqualToString:@"CodeCatalogue"] ) {
        NSError *error;
        [ appDelegate.managedObjectContext save: &error ];
        
        if ( finishedDelegate != nil )
            [ finishedDelegate performSelector: finishedSelector withObject: currentCatalogue ];
        
        currentSubPart = nil;
		return;
    }
    else {
		currentSubPart = nil;
	}
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    
    if ( [currentSubPart isEqualToString:@"Id"] ) {
        currentCatalogue.catalogueId = [ self parseStringToNumber: string ];
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"Name"] ) {
        
        currentCatalogue.name = string;
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"Type"]) {
        currentCatalogue.type = string;
        return;
    }
    
    
}

- (NSDate*) parseRFC3339DateTimeString:(NSString *)rfc3339DateTimeString
// Returns a user-visible date time string that corresponds to the
// specified RFC 3339 date time string. Note that this does not handle
// all possible RFC 3339 date time strings, just one of the most common
// styles.
{
    NSString *          userVisibleDateTimeString;
    NSDateFormatter *   rfc3339DateFormatter;
    NSLocale *          enUSPOSIXLocale;
    
    userVisibleDateTimeString = nil;
    
    // Convert the RFC 3339 date time string to an NSDate.
    
    rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    
    enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    return [rfc3339DateFormatter dateFromString:rfc3339DateTimeString];
}

- (NSNumber*) parseStringToNumber:(NSString*)string {
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    return [f numberFromString:string];
}

@end
