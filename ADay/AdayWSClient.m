//
//  AdayWSClient.m
//  Aday
//
//  Created by Stefan Norberg on 2011-11-07.
//  Copyright (c) 2011 Medc AB. All rights reserved.
//

#import "AdayWSClient.h"
#import "NSData+Base64.h"

@interface AdayWSClient()

@end

@implementation AdayWSClient
@synthesize conn,data;

//NSString *const WEBSERVICE_ENDPOINT = @"http://test3.medc.se/AdayWS/MobileService.svc"; // For testing and debugging purposes
NSString *const WEBSERVICE_ENDPOINT = @"http://aday.medc.se/AdayWS/MobileService.svc";

-(NSString*) getAuthorizationHeader:(NSString*) username password:(NSString*) password {   
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    return [NSString stringWithFormat:@"Basic %@", [authData base64EncodingWithLineLength:80]];
}

#pragma mark - NSURLConnection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    
    //if ( self.conn != connection )
      //  return;
    
    if ( self.data != nil )
        [self.data setLength:0];
    
    NSLog(@"Resetting");
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)dataChunk
{
   // if ( self.conn != connection )
  //      return;
    
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [self.data appendData:dataChunk];
    NSLog(@"Appending data: %lu", (unsigned long)[dataChunk length] );
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    //[connection release];
    // connection = nil;
    // receivedData is declared as a method instance elsewhere
    
   // if ( self.conn != connection )
  //      return;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if ( self.conn != connection )
        return;
    
    
    NSString *str = [ [ NSString alloc ] initWithData: self.data encoding: NSUTF8StringEncoding ];
    
    NSLog(@"Succeeded! Received %lu bytes of data: %@",(unsigned long)[self.data length], str  );
    
    
    [ self finishedWithData: data ];
  
    
    [ str release ];
}

-(void) finishedWithData: (NSData *) data {
}


@end
