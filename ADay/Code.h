//
//  Code.h
//  ADay
//
//  Created by Ronnie Persson on 2012-03-26.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Catalogue;

@interface Code : NSManagedObject

@property (nonatomic, retain) NSString * catalogueid;
@property (nonatomic, retain) NSString * codeId;
@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSString * tags;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) Catalogue *catalogue;

@end
