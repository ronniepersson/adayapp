//
//  SignInViewController.m
//  ADay
//
//  Created by Ronnie Persson on 2011-12-22.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "SignInViewController.h"
#import "Code.h"
#import "Catalogue.h"
#import "Map.h"

@implementation SignInViewController
@synthesize signIn;
@synthesize signInButton;
@synthesize demoButton;
@synthesize activity;
@synthesize  mapWS,catalogueWS,codeWS;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void) subCatalog
{
    nrOfCataloges--;
    
    if ( nrOfCataloges <= 0 )
    {
        NSUserDefaults *defaults = [ NSUserDefaults standardUserDefaults ];
        
        [ defaults setValue: self.signIn.text forKey: @"mapName" ];
        [ defaults synchronize ];
        
        [ [ NSNotificationCenter defaultCenter ] postNotificationName: @"ADAYCodesIsUpdated" object: nil ];
        
       
        
        [ self dismissViewControllerAnimated: YES completion:^{
            
        } ];
    }
}

-(void) addCatalog
{
    nrOfCataloges++;
}

-(void) finishedDownloadingCode: ( Code *) code
{
    
}

-( void ) finishedDownloadingCatalogue: ( Catalogue * ) catalogue
{
    [ self addCatalog ];
    self.codeWS = [ [ ADAYCodeWS alloc ] init ];
    codeWS.finishedDelegate = self;
    codeWS.finishedSelector = @selector( finishedDownloadingCode: );
    codeWS.finishedAllSelector = @selector( subCatalog );
    [ self.activity startAnimating ];
    [ codeWS getCodesInCatalogueForProject:catalogue Map: mapWS.currentMap ];
}

-( void ) finishedDownloadingMap: ( Map * ) map
{
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    
    appDelegate.map = map;
    
    self.catalogueWS = [ [ ADAYCatalogueWS alloc ] init ];
    catalogueWS.finishedDelegate = self;
    catalogueWS.finishedSelector = @selector( finishedDownloadingCatalogue: );
    [ self.activity startAnimating ];
    [ catalogueWS getCatalogesForProject: map ];
    
}

-(void ) signInError
{
    self.activity.hidden = YES;
    self.signInButton.enabled = YES;
    UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: NSLocalizedString( @"Error", @"Error") message: NSLocalizedString( @"Wrong key", @"Sign in - wrong key") delegate: nil cancelButtonTitle: nil  otherButtonTitles: NSLocalizedString( @"Okay", @"" ), nil ];
    [ alert show ];
    [ alert release ];
    
    [self.signIn becomeFirstResponder ];
}

#pragma mark - UITextField delegate

-(void) textFieldDidEndEditing:(UITextField *)textField
{
}

-(BOOL ) textFieldShouldReturn:(UITextField *)textField
{
    
    [self.signIn resignFirstResponder ];
    return YES;
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    nrOfCataloges = 0;
    
    //UI
    self.signIn.delegate = self;
    
     [ [ NSNotificationCenter defaultCenter ] addObserver: self selector: @selector( signInError) name:@"ADAYSignInError" object: nil  ];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setActivity:nil];
    [self setSignIn:nil];
    [self setSignInButton:nil];
    [self setDemoButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [activity release];
    [signIn release];
    [signInButton release];
    [demoButton release];
    [super dealloc];
}
- (IBAction)signIn:(id)sender {
   
    
    //User must fill in code
    if ( [self.signIn.text length] > 0 )
    {
        [ self signInWithCode: self.signIn.text ];
      } else {
        [self.signIn becomeFirstResponder];
    }
    
}

- (IBAction)demoSignIn:(id)sender {
    [ self signInWithCode: @"DEMO" ];
}

-(void) signInWithCode: ( NSString * ) code
{
    
    signInButton.enabled = NO;
    demoButton.enabled = NO;
    
    [ self.signIn resignFirstResponder ];
    
    nrOfCataloges = 0;
    self.mapWS = [[ ADAYMapWS alloc ] init ];
    mapWS.finishedDelegate = self;
    mapWS.finishedSelector = @selector( finishedDownloadingMap: );
    [ mapWS setMapWithName: code ];
    
    
    self.activity.hidden = NO;
    [self.activity startAnimating];

}

@end
