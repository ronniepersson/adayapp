//
//  ChooseTimeViewController.h
//  ADay
//
//  Created by Ronnie Persson on 2012-04-20.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Registration.h"

@interface ChooseTimeViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIDatePicker *timePicker;

@property (retain, nonatomic) Registration *registration;
@property (retain, nonatomic) IBOutlet UILabel *timeLabel;

- (IBAction)timeChanged:(id)sender;
- (IBAction)done:(id)sender;

@end
