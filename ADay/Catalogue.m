//
//  Catalogue.m
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "Catalogue.h"


@implementation Catalogue

@dynamic catalogueId;
@dynamic name;
@dynamic type;

@end
