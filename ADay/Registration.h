//
//  Registration.h
//  ADay
//
//  Created by Ronnie Persson on 2012-03-23.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Map;

@interface Registration : NSManagedObject

@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSNumber * durationInMinutes;
@property (nonatomic, retain) NSString * activityCode1;
@property (nonatomic, retain) NSString * activityCode2;
@property (nonatomic, retain) NSString * locationCode;
@property (nonatomic, retain) NSString * togetherCode1;
@property (nonatomic, retain) NSString * togetherCode2;
@property (nonatomic, retain) NSString * togetherCode3;
@property (nonatomic, retain) NSNumber * moodCode;
@property (nonatomic, retain) Map *map;

@end
