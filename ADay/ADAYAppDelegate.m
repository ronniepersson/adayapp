//
//  ADAYAppDelegate.m
//  ADay
//
//  Created by Ronnie Persson on 2011-11-21.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "ADAYAppDelegate.h"
#import "ADAYViewController.h"

@implementation ADAYAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;
@synthesize map;
@synthesize insertRegistations;

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

#pragma mark -
#pragma mark Application's Documents directory

/**
 Returns the path to the application's Documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the 
 persistent store coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext {
    
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = 
    [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [ managedObjectContext setMergePolicy: NSMergeByPropertyObjectTrumpMergePolicy ];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    
    return managedObjectContext;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models
 found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [[NSManagedObjectModel 
                           mergedModelFromBundles:nil] retain];    
    return managedObjectModel;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's 
 store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    
    NSURL *storeUrl = [NSURL fileURLWithPath: 
                       [[self applicationDocumentsDirectory] 
                        stringByAppendingPathComponent: @"ADAYClient.sqlite"]];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] 
                                  initWithManagedObjectModel:[self managedObjectModel]];
    
    NSDictionary *options = [ NSDictionary dictionaryWithObjectsAndKeys: [ NSNumber numberWithBool: YES ],  NSMigratePersistentStoresAutomaticallyOption, [ NSNumber numberWithBool: YES ], NSInferMappingModelAutomaticallyOption, nil ];
    
    if (![persistentStoreCoordinator 
          addPersistentStoreWithType:NSSQLiteStoreType
          configuration:nil URL:storeUrl 
          options: options
          error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    
    
    return persistentStoreCoordinator;
}

+ (NSString *)GetUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return [(NSString *)string autorelease];
}

-( NSString * ) getGuid
{
    NSUserDefaults *defaults = [ NSUserDefaults standardUserDefaults ];
    if ( [ defaults valueForKey: @"GUID" ] == nil )
    {
        CFUUIDRef theUUID = CFUUIDCreate(NULL);
        CFStringRef string = CFUUIDCreateString(NULL, theUUID);
        CFRelease(theUUID);
        [ defaults setValue: (NSString *)string forKey: @"GUID" ];
        [ defaults synchronize ];
    } 
    
    return  [ defaults valueForKey: @"GUID" ];
}

#pragma mark - App Lifecykle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSUserDefaults *defaults = [ NSUserDefaults standardUserDefaults ];
    
    if ( [ defaults valueForKey: @"mapName" ] == nil )
    {
    } else
    {
        NSEntityDescription *entity = [NSEntityDescription 
                                       entityForName:@"Map"
                                       inManagedObjectContext: self.managedObjectContext];
        
        NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
        
        [request setEntity:entity];
        
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                            initWithKey:@"mapId" ascending: YES ];
        
        [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        [sortDescriptor release];
        
        NSError *error = nil;
        NSArray *maps =  [ self.managedObjectContext executeFetchRequest:request error:&error ] ;
        self.map = [ maps objectAtIndex: 0 ];
    }
    
    self.insertRegistations = [ [ ADAYInsertRegistrationsWS alloc ] init ];
    
    [ [ NSNotificationCenter defaultCenter ] addObserver: self selector: @selector(sendRegistrations) name:@"ADAYNeedSyncingOfRegistrations" object: nil ];

    return YES;
}

-(void )sendRegistrations
{
    [ self.insertRegistations insertRegistrationsForMap: self.map ]; 
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
