//
//  ADAYViewController.m
//  ADay
//
//  Created by Ronnie Persson on 2011-11-21.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "ADAYViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Registration+Extended.h"
#import "ChooseTimeViewController.h"
#import "InfoViewController.h"

@interface ADAYViewController()
-(void) updateReadyState;
-(void) setReady;
-(void) setNotReady;
@property int keyboardHeight;
@property int selectedRowInPicker;
@property int normalYForWordsTable;
@end

@implementation ADAYViewController
@synthesize words,matchedWords;
@synthesize input;
@synthesize buttonView;
@synthesize codes;
@synthesize wordButtons;
@synthesize pause;
@synthesize smileyButton;
@synthesize submitButton;
@synthesize smileyPicker;
@synthesize whatButton;
@synthesize whereButton;
@synthesize whoButton;
@synthesize registrations;
@synthesize changesTextByClicking;
@synthesize smiley;
@synthesize reg;
@synthesize selectedRowInPicker;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}



-(NSRange) rangeForWordAtLocation: (int) location inString: (NSString *) str
{
    NSRange lastSpace = [ str rangeOfString: @" " options: NSBackwardsSearch range: NSMakeRange( 0, location ) ];
    
    
    NSRange nextSpace = [ str rangeOfString: @" " options: NSCaseInsensitiveSearch range: NSMakeRange ( location , [ str length ] - location ) ];
    
    
    int startPosOfWord = 0;
    if ( lastSpace.location != NSNotFound )
        startPosOfWord = (int)lastSpace.location + 1;
    
    int endPosOfWord = (int)[str length];
    if ( nextSpace.location != NSNotFound )
        endPosOfWord = (int)nextSpace.location;
	
    return NSMakeRange( startPosOfWord, endPosOfWord - startPosOfWord );
}

-(NSString *) wordAtLocation: (int) location inString:(NSString *)str 
{
    NSRange range = [ self rangeForWordAtLocation: location inString: str ];
    
    
    
    return [ str substringWithRange: range ];
}

-(void) setupCodes
{
    
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    
    NSEntityDescription *entity = [NSEntityDescription 
                                   entityForName:@"Code"
                                   inManagedObjectContext: appDelegate.managedObjectContext];
    
    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    
    [request setEntity:entity];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"text" ascending: YES ];
    
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    
    NSError *error = nil;
    self.codes = [ appDelegate.managedObjectContext executeFetchRequest:request error:&error ];
    
    if ( self.codes == nil )
    {
        NSLog( @"Error in sql: %@", [ error description ] );
        return;
    }
    
    if ( [ self.codes count ] == 0 )
    {
        NSLog( @"No codes" );
    }
    
    

}

-(void) updateReadyState
{
    if ( [ self hasActivity ] && self.smiley != -1 )
    {
        [ self setReady ];
    } else
    {
        [ self setNotReady ];
    }
}

-(void) setReady
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

-(void) setNotReady
{
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

#pragma mark - UIScrollView delegate

-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    for ( TextButton *b in wordButtons )
        [ b setNeedsDisplay ];
}

#pragma mark - TextField Delegate


-(void) textViewDidBeginEditing:(UITextView *)textView
{
    [ self.smileyPicker setHidden: YES ];
}

-(void) textViewDidChange:(UITextView *)textView
{
    NSString *word =  [ self wordAtLocation: (int)textView.selectedRange.location inString: textView.text ];
    
   
    [ self updateReadyState ];
    
    if ( self.changesTextByClicking )
    {
        self.changesTextByClicking = NO;
        return;
    }
    
    if (!pause )
    {
        NSPredicate *bPredicate =
        [NSPredicate predicateWithFormat:@"SELF.text beginswith[c] %@ OR SELF.tags contains[c] %@", word, word ];
     
        
        self.matchedWords = [ codes filteredArrayUsingPredicate:bPredicate];
        
        
        if ( [self.matchedWords count] > 0 ){
            [ self layoutWithKeyboard ];
            words.hidden = NO;
        }
        else
        {
            //[ self layoutWithoutKeyboard ];
            words.hidden = YES;
        }
        [ words reloadData ];
    }
    
    [ self updateReadyState ];
    
    
}

-(void ) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    /*if ( !self.words.hidden )
     self.input.frame = CGRectMake( 3, 47, self.input.frame.size.width, 105 -47 );
     else
     self.input.frame = CGRectMake( 3, 47, self.input.frame.size.width, 171 );*/
}


#pragma mark UITableViewDataSource

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ self.matchedWords count ];
}


-( UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellMatched";
    
    UITableViewCell *cell = ( UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    Code *code = [ self.matchedWords objectAtIndex: [ indexPath row ] ];
    cell.textLabel.text = code.text;
    
    //cell.textLabel.backgroundColor = [ self colorForType: code.catalogue.type ];
    
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
    
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Code *code = [ self.matchedWords objectAtIndex: [ indexPath row ] ];
    cell.backgroundColor = [ self colorForType: code.catalogue.type ];
    
}



#pragma mark UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Code *code = [ self.matchedWords objectAtIndex: [ indexPath row ] ];
   
        int location = (int)input.selectedRange.location;
        NSRange rangeForWord = [ self rangeForWordAtLocation: location inString: input.text ];
        
        
        NSString *replaceWord = code.text;
        input.text = [ input.text stringByReplacingCharactersInRange: rangeForWord withString: replaceWord  ];
        
        input.selectedRange = NSMakeRange( rangeForWord.location + replaceWord.length , 0 );
        
        
        
        
        TextButton *wordButton = [[ TextButton alloc ] initWithTextField: self.input andCode: code ];
        wordButton.delegateController = self;
        wordButton.code = code;
        
         
        [ wordButton textUpdated: nil ];
        [ self.view addSubview: wordButton ];
        [ wordButtons addObject: wordButton ];
        [ wordButton release ];
        
        self.input.text = [ NSString stringWithFormat: @"%@ ", self.input.text ];

    
    self.changesTextByClicking = YES;
    self.words.hidden = YES;
    
    [ self.input becomeFirstResponder ];
    [ self updateReadyState ];
    
    [ [ NSNotificationCenter defaultCenter ] postNotificationName: @"ADAYWordWasReplaced" object: nil ];
    
    
}

#pragma mark - Picker datasource delegate


-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 5;
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 74;
}


-(UIView *) pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UIImageView *imageView = [ [ UIImageView alloc ] init ];
    imageView.frame=CGRectMake(0, 5, 64, 64);
    imageView.image = [ UIImage imageNamed: [ NSString stringWithFormat: @"snurra_%ld.png", (long)5-row ] ];
    return imageView;
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [ self.smileyButton setImage: [ UIImage imageNamed: [ NSString stringWithFormat: @"smiley_%ld.png", 5-row ] ] forState:UIControlStateNormal ];
    [ self.smileyButton setImage: [ UIImage imageNamed: [ NSString stringWithFormat: @"smiley_%ld_a.png", 5-row ] ] forState:UIControlStateHighlighted ];
    
    self.smiley = 5-(int)row;
    self.selectedRowInPicker = (int)row;
    NSLog( @"Row: %ld", 5-row );
    
    [ self updateReadyState ];
    
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.matchedWords = [[ NSArray alloc ] init];
    self.codes = [[ NSMutableArray alloc ] initWithCapacity: 10];
    self.reg = nil;
    self.smiley = -1;
    self.selectedRowInPicker = -1;
    
    input.layer.shadowColor = [ UIColor blackColor ].CGColor;
    input.layer.shadowOffset = CGSizeMake( 2,2 );
    input.layer.shadowOpacity = 0.4;
    input.clipsToBounds = YES;
    
    self.normalYForWordsTable = self.words.frame.origin.y;
    
    [ input becomeFirstResponder ];
    
    self.wordButtons = [ NSMutableArray arrayWithCapacity: 10 ];
    
    [ self setupCodes ];
    
    [ [ NSNotificationCenter defaultCenter ] addObserver: self selector: @selector( setupCodes ) name: @"ADAYCodesIsUpdated" object: nil ];
    
    [ [ NSNotificationCenter defaultCenter ] addObserver: self selector: @selector( clear: ) name: @"ADAYCodeIsSubmitted" object: nil ];
    
    [ [ NSNotificationCenter defaultCenter ] addObserver: self selector: @selector( updateReadyState ) name: @"ADAYTextButtonDead" object: nil ];
    
    [ [ NSNotificationCenter defaultCenter ] addObserver: self selector: @selector( keyboardWasShown: ) name: UIKeyboardDidShowNotification object: nil ];
    
    [ [ NSNotificationCenter defaultCenter ] addObserver: self selector: @selector( keyboardWasHidden: ) name: UIKeyboardDidHideNotification object: nil ];
    
    [ self addObserver: self forKeyPath: @"words.hidden" options: NSKeyValueObservingOptionNew  context: nil ];
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
                             


    
    [ self updateReadyState ];
    
    self.navigationItem.title = NSLocalizedString( @"What are you doing?", @"What are you doing?" );
    self.navigationItem.leftBarButtonItem.title = NSLocalizedString(@"Help",@"Help button");
    
    
//    NSArray *languages = [NSLocale preferredLanguages];
    
//    //Language
//    if ( ![ [languages objectAtIndex: 0 ] isEqualToString: @"sv" ] )
//    {
//        [self.whoButton setImage: [ UIImage imageNamed: @"knapp_with.png" ] forState: UIControlStateNormal ];
//        [self.whoButton setImage: [ UIImage imageNamed: @"knapp_with_a.png" ] forState: UIControlStateHighlighted ];
//        
//        
//        [self.whereButton setImage: [ UIImage imageNamed: @"knapp_where.png" ] forState: UIControlStateNormal ];
//        [self.whereButton setImage: [ UIImage imageNamed: @"knapp_where_a.png" ] forState: UIControlStateHighlighted ];
//        
//        
//        [self.whatButton setImage: [ UIImage imageNamed: @"knapp_what.png" ] forState: UIControlStateNormal ];
//        [self.whatButton setImage: [ UIImage imageNamed: @"knapp_what_a.png" ] forState: UIControlStateHighlighted ];
//    }
	
	// Do any additional setup after loading the view, typically from a nib.
    
    NSUserDefaults *defaults = [ NSUserDefaults standardUserDefaults ];
    
    if ( [ defaults valueForKey: @"mapName" ] == nil )
    {
        [ self performSegueWithIdentifier: @"signin" sender: self ];
    }
}




-(BOOL) hasActivity
{
    for ( TextButton *tb in wordButtons )
    {
        if ( [tb.code.catalogue.type isEqualToString: @"Activity" ] && !tb.hidden )
            return YES;
    }
    
    return NO;
}

-(BOOL) hasLocation
{
    for ( TextButton *tb in wordButtons )
    {
        if ( [tb.code.catalogue.type isEqualToString: @"Location" ] && !tb.hidden  )
            return YES;
    }
    
    return NO;
}

-(BOOL) hasTogether
{
    for ( TextButton *tb in wordButtons )
    {
        if ( [tb.code.catalogue.type isEqualToString: @"Together" ] && !tb.hidden  )
            return YES;
    }
    
    return NO;
}
- (void)viewDidUnload
{
    [self setWords:nil];
    [self setInput:nil];
    [self setButtonView:nil];
    [self setSmileyPicker:nil];
    [self setSmileyButton:nil];
    [self setSubmitButton:nil];
    [self setWhatButton:nil];
    [self setWhereButton:nil];
    [self setWhoButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [words release];
    [input release];
    [buttonView release];
    [smileyPicker release];
    [smileyButton release];
    [submitButton release];
    [whatButton release];
    [whereButton release];
    [whoButton release];
    [super dealloc];
}

- (void) keyboardWasShown:(NSNotification *)nsNotification {
    NSDictionary *userInfo = [nsNotification userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.keyboardHeight = kbSize.height;
    NSLog(@"Height: %f Width: %f", kbSize.height, kbSize.width);
    // Portrait:    Height: 264.000000  Width: 768.000000
    // Landscape:   Height: 1024.000000 Width: 352.000000
    
    [ self layoutWithKeyboard ];
}

- (void) keyboardWasHidden:(NSNotification *)nsNotification {
   
    [ self layoutWithoutKeyboard ];
}


-(void) layoutWithKeyboard
{
    int diff =  self.view.frame.size.height - self.words.frame.origin.y - self.keyboardHeight;
    self.input.frame = CGRectMake( 5, 65, self.input.frame.size.width, 171 );
    
    if ( diff > 100 )
    {
        self.words.frame = CGRectMake( 0, self.normalYForWordsTable , 320, diff );
    } else {
        self.words.frame = CGRectMake( 0, self.normalYForWordsTable - 64 , 320, diff + 64 );
    }
}

-(void) layoutWithoutKeyboard
{
    int diff =  self.view.frame.size.height - self.words.frame.origin.y;
    
    self.input.frame = CGRectMake( 5, 65, self.input.frame.size.width, 171 );
    self.words.frame = CGRectMake( 0, self.normalYForWordsTable , 320, diff );
}



-(UIColor*) colorForType: ( NSString *) type
{
    if ( [ type isEqualToString: @"Activity" ] )
    {
        return [ UIColor colorWithRed: 255.0 / 255.0 green: 204.0 / 255.0 blue: 204.0 / 255.0 alpha: 1.0 ];
    }
    
    if ( [ type isEqualToString: @"Location" ] )
    {
        return [ UIColor colorWithRed: 204.0 / 255.0 green: 204.0 / 255.0 blue: 255.0 / 255.0 alpha: 1.0 ];
    }
    
    //With who
   return [ UIColor colorWithRed: 204.0 / 255.0 green: 255.0 / 255.0 blue: 204.0 / 255.0 alpha: 1.0 ]; 
}



- (IBAction)showWordsForTogether:(id)sender {
    
    
    if ( [ self wordAtLocation: (int)self.input.selectedRange.location inString: self.input.text ].length > 1 ){
        input.text = [ input.text stringByReplacingCharactersInRange: self.input.selectedRange withString: @" "  ];
        
        self.input.selectedRange = NSMakeRange( self.input.selectedRange.location+1  , self.input.selectedRange.length  );
    }
    
    //[ self layoutWithoutKeyboard ];
    
    [ self.input resignFirstResponder ];

    
    NSPredicate *bPredicate =
    [NSPredicate predicateWithFormat:@"SELF.catalogue.type = 'Together'" ];
    
    self.matchedWords = [ codes filteredArrayUsingPredicate:bPredicate];
    
    if ( [self.matchedWords count] > 0 )
        words.hidden = NO;
    else
        words.hidden = YES;
    
    [ words reloadData ];
}

- (IBAction)showWordsForLocation:(id)sender {
    
    if ( [ self wordAtLocation: (int)self.input.selectedRange.location inString: self.input.text ].length > 1 ){
        input.text = [ input.text stringByReplacingCharactersInRange: self.input.selectedRange withString: @" "  ];
        
        self.input.selectedRange = NSMakeRange( self.input.selectedRange.location+1  , self.input.selectedRange.length  );
    }
    
    
    //[ self layoutWithoutKeyboard ];
    
    [ self.input resignFirstResponder ];
    
    NSPredicate *bPredicate =
    [NSPredicate predicateWithFormat:@"SELF.catalogue.type = 'Location'" ];
    
    self.matchedWords = [ codes filteredArrayUsingPredicate:bPredicate];
    
    if ( [self.matchedWords count] > 0 )
        words.hidden = NO;
    else
        words.hidden = YES;
    
    [ words reloadData ];
}

- (IBAction)showWordsForActivity:(id)sender {
    
    
    if ( [ self wordAtLocation: (int)self.input.selectedRange.location inString: self.input.text ].length > 1 ){
        input.text = [ input.text stringByReplacingCharactersInRange: self.input.selectedRange withString: @" "  ];
        
        self.input.selectedRange = NSMakeRange( self.input.selectedRange.location+1  , self.input.selectedRange.length  );
    }
    
    //[ self layoutWithoutKeyboard ];
    [ self.input resignFirstResponder ];
    
    
    NSPredicate *bPredicate =
    [NSPredicate predicateWithFormat:@"SELF.catalogue.type = 'Activity'" ];
    
    self.matchedWords = [ codes filteredArrayUsingPredicate:bPredicate];
    
    if ( [self.matchedWords count] > 0 )
        words.hidden = NO;
    else
        words.hidden = YES;
    
    [ words reloadData ];
    
}

- (IBAction)clear:(id)sender {
  
    
    for ( UIView *v in [self.view subviews ] )
    {
        if ( [ v isKindOfClass: [ TextButton class ] ] )
            [ v removeFromSuperview ];
    }
    
    self.reg = nil;
    
    self.smiley = -1;
    self.selectedRowInPicker = -1;
    
    [ self.smileyButton setImage: [ UIImage imageNamed: [ NSString stringWithFormat: @"smiley_0.png"] ] forState:UIControlStateNormal ];
    [ self.smileyButton setImage: [ UIImage imageNamed: [ NSString stringWithFormat: @"smiley_0_a.png" ] ] forState:UIControlStateHighlighted ];
    

    self.wordButtons = [[ NSMutableArray alloc ] initWithCapacity:10];
    self.input.text = @"";
    
    [ self.input becomeFirstResponder ];
    
    
    [ self updateReadyState ];
}

- (IBAction)submit:(id)sender {
    
    
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    
    NSMutableArray *b = [[ NSMutableArray alloc ] initWithCapacity:10];
    for ( UIView *v in [self.view subviews ] )
    {
        if ( [ v isKindOfClass: [ TextButton class ] ] )
        {
            [ b addObject: ( TextButton *) v ];
        }
    }
    
    int activityNr = 0;
    int togetherNr = 0;
    
   
    self.reg = [ Registration insertRegistrationWithMap: appDelegate.map ];
    
    
    reg.activityCode1 = @"";
    reg.activityCode2 = @"";
    reg.locationCode = @"";
    reg.togetherCode1 = @"";
    reg.togetherCode2 = @"";
    reg.togetherCode3 = @"";
    
    
    for ( TextButton *textButton in wordButtons )
    {
        if ( !textButton.hidden )
        {
        if ( [ textButton.code.catalogue.type isEqualToString: @"Activity" ] )
        {
            switch ( activityNr ) {
                case 0:
                    reg.activityCode1 = [ NSString stringWithFormat: @"%@", textButton.code.codeId ];
                    reg.activityCode2 = @"";
                    break;
                    
                case 1:
                    reg.activityCode2 = [ NSString stringWithFormat: @"%@", textButton.code.codeId ];
                    break;
                default:
                    break;
            }
            activityNr++;
        } else if ( [ textButton.code.catalogue.type isEqualToString: @"Together" ] )
        {
            switch ( togetherNr ) {
                case 0:
                    reg.togetherCode1 = [ NSString stringWithFormat: @"%@", textButton.code.codeId ];
                    break;
                    
                case 1:
                    reg.togetherCode2 = [ NSString stringWithFormat: @"%@", textButton.code.codeId ];
                    break;
                case 2:
                    reg.togetherCode3 = [ NSString stringWithFormat: @"%@", textButton.code.codeId ];
                    break;
                default:
                    break;
            }
            togetherNr++;
        } else //Location
        {
            reg.locationCode = [ NSString stringWithFormat: @"%@", textButton.code.codeId ];
        }
    }
    }
    reg.moodCode = [ NSNumber numberWithInt: self.smiley + 1 ];
    reg.timeStamp = [ NSDate date ];
    
    NSError *error;
    [ appDelegate.managedObjectContext save: &error ];
    

    [ self performSegueWithIdentifier: @"save" sender: self ];
}

-( void ) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [ segue.destinationViewController isKindOfClass: [ ChooseTimeViewController class ] ] )
    {
        ChooseTimeViewController *ctvc = ( ChooseTimeViewController * ) segue.destinationViewController;
        ctvc.registration = self.reg;
    }
}



- (IBAction)showSmileyPicker:(id)sender {
    [ self.input resignFirstResponder ];
    self.smileyPicker.hidden = NO;
    if ( self.selectedRowInPicker == -1 )
        self.selectedRowInPicker = 2;
    else {
        
    }
    
    
    [self.smileyPicker selectRow: selectedRowInPicker inComponent:0 animated: YES ];
    [ self pickerView: self.smileyPicker didSelectRow: selectedRowInPicker inComponent: 0 ];
    [ self.words setHidden: YES ];
}
@end
