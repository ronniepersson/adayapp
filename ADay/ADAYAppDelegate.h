//
//  ADAYAppDelegate.h
//  ADay
//
//  Created by Ronnie Persson on 2011-11-21.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ADAYInsertRegistrationsWS.h"

#import "Map.h"

@class ADAYViewController;

@interface ADAYAppDelegate : UIResponder <UIApplicationDelegate>
{
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;           
    NSPersistentStoreCoordinator *persistentStoreCoordinator; 
}


@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic,retain) ADAYInsertRegistrationsWS *insertRegistations;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ADAYViewController *viewController;
@property ( nonatomic, retain ) Map *map;

+ (NSString *)GetUUID;
- (NSManagedObjectContext *) managedObjectContext;
-( NSString * ) getGuid;

@end
