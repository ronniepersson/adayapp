//
//  ADAYMapWS.m
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "ADAYMapWS.h"
#import "NSData+Base64.h"

@implementation ADAYMapWS
@synthesize currentMap,finishedDelegate,finishedSelector;

-(void)setMapWithName: (NSString *)name
{    
	NSURL* url = [[NSURL alloc] initWithString: [ NSString stringWithFormat: @"%@/GetMap?mobilekey=%@", WEBSERVICE_ENDPOINT, name ] ];
    
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL: url];
	[request addValue: [self getAuthorizationHeader:@"ronnie" password:@"123"] forHTTPHeaderField:@"Authorization"];
    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:[ self retain ] startImmediately: NO ];
    
    self.conn = connection;
    [ connection release ];
    
    self.data = [[ NSMutableData alloc ] init ];
	
    
    [ self.conn start ];
	[ request release ];
    [ url release ];
}


-(void) finishedWithData:(NSData *)data
{
    NSString *dataStr = [[ NSString alloc ] initWithData: data encoding: NSUTF8StringEncoding ] ;
    NSLog( @"data %@", dataStr );
    
    if ( [dataStr length]== 0)
    {
        [[ NSNotificationCenter defaultCenter ] postNotificationName: @"ADAYSignInError" object: nil ];
        return;
    }
    
    // Load the xml answer
    NSXMLParser* parser = [[NSXMLParser alloc] initWithData:data];
    [parser setDelegate:(id)self];
    [parser setShouldResolveExternalEntities:YES];
    [ parser parse ];
    
    if ( self.finishedDelegate != nil )
        [ self.finishedDelegate performSelector: finishedSelector withObject: currentMap ];

}

#pragma mark - XMLParser delegate

-(void) parserDidStartDocument:(NSXMLParser *)parser
{
    NSLog( @"who" );
}

-(void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog( @"Error" );
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
	
	NSLog(@"element Name: %@, ", elementName);
    
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    NSEntityDescription *entity;
    
	
    if ( [elementName isEqualToString:@"Map"] ) {
        entity = [ NSEntityDescription entityForName: @"Map" inManagedObjectContext: appDelegate.managedObjectContext ];
        
        self.currentMap = [[ Map alloc ] initWithEntity: entity insertIntoManagedObjectContext: appDelegate.managedObjectContext ];
        return;
    } 
    else {
		currentSubPart = elementName;
	}
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    
	// Del is the main object
    if ( [elementName isEqualToString:@"Map"]) {
        NSError *error;
        [ appDelegate.managedObjectContext save: &error ];
		
        
		currentSubPart = nil;
        
        
		return;
    } 
    else {
		currentSubPart = nil;
	}
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ( [currentSubPart isEqualToString:@"Id"] ) {
        currentMap.mapId = string;
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"StartDate"] ) {
        
        currentMap.startDate = [ self parseRFC3339DateTimeString: string ];
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"EndDate"]) {
        currentMap.endDate = [ self parseRFC3339DateTimeString: string ];
        return;
    }
    
    
    if ( [currentSubPart isEqualToString:@"Lang"]) {
        currentMap.language = string;
        return;
    }
    
    
    if ( [currentSubPart isEqualToString:@"Name"] ) {
        currentMap.name = string;
        return;
    }
    
    
    if ( [currentSubPart isEqualToString:@"ProjectId"] ) {
        currentMap.projectid = [ self parseStringToNumber: string ];
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"Description"] ) {
        currentMap.descriptionText = string;
        return;
    }
    
}

- (NSDate*) parseRFC3339DateTimeString:(NSString *)rfc3339DateTimeString
// Returns a user-visible date time string that corresponds to the
// specified RFC 3339 date time string. Note that this does not handle
// all possible RFC 3339 date time strings, just one of the most common
// styles.
{
    NSString *          userVisibleDateTimeString;
    NSDateFormatter *   rfc3339DateFormatter;
    NSLocale *          enUSPOSIXLocale;
    
    userVisibleDateTimeString = nil;
    
    // Convert the RFC 3339 date time string to an NSDate.
    
    rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    
    enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    return [rfc3339DateFormatter dateFromString:rfc3339DateTimeString];
}

- (NSNumber*) parseStringToNumber:(NSString*)string {
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    return [f numberFromString:string];
}


@end
