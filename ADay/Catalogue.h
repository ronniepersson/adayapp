//
//  Catalogue.h
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Catalogue : NSManagedObject

@property (nonatomic, retain) NSNumber * catalogueId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * type;

@end
