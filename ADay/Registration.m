//
//  Registration.m
//  ADay
//
//  Created by Ronnie Persson on 2012-03-23.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "Registration.h"
#import "Map.h"


@implementation Registration

@dynamic timeStamp;
@dynamic durationInMinutes;
@dynamic activityCode1;
@dynamic activityCode2;
@dynamic locationCode;
@dynamic togetherCode1;
@dynamic togetherCode2;
@dynamic togetherCode3;
@dynamic moodCode;
@dynamic map;

@end
