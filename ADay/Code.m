//
//  Code.m
//  ADay
//
//  Created by Ronnie Persson on 2012-03-26.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "Code.h"
#import "Catalogue.h"


@implementation Code

@dynamic catalogueid;
@dynamic codeId;
@dynamic language;
@dynamic tags;
@dynamic text;
@dynamic catalogue;

@end
