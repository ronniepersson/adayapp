//
//  ADAYCodeWS.m
//  ADay
//
//  Created by Ronnie Persson on 2011-11-23.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import "ADAYCodeWS.h"
#import "Map.h"

@implementation ADAYCodeWS
@synthesize currentCode,finishedDelegate,finishedSelector,finishedAllSelector, catalogue,codes;

@synthesize currentString;

//-(void) getCodesForCatalogue: ( Catalogue *) cat Map: ( Map *) map
//{
//    self.catalogue = cat;
//    self.codes = [ self.catalogue mutableSetValueForKey: @"codes" ];
//    
//    NSArray *languages = [NSLocale preferredLanguages];
//    NSString *langCode = [ languages objectAtIndex: 0 ];
//    
//    NSURL* url = [[NSURL alloc] initWithString: [ NSString stringWithFormat: @"%@/GetCodes?catalogueid=%@&lang=%@", WEBSERVICE_ENDPOINT, catalogue.catalogueId, langCode ] ];
//    
//	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
//	[request addValue: [ self getAuthorizationHeader:@"ronnie" password:@"123"]   forHTTPHeaderField:@"Authorization"];
//    
//    
//    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:[ self retain ] startImmediately: NO ];
//    
//    self.conn = connection;
//    [ connection release ];
//    
//    self.data = [[ NSMutableData alloc ] init ];
//	
//    [ self.conn start ];
//	[ request release ];
//    [ url release ];
//    
//}

-(void) getCodesInCatalogueForProject: ( Catalogue *) cat Map: ( Map *) map
{
    self.catalogue = cat;
    self.codes = [ self.catalogue mutableSetValueForKey: @"codes" ];
    
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *langCode = [ languages objectAtIndex: 0 ];
    
    NSURL* url = [[NSURL alloc] initWithString: [ NSString stringWithFormat: @"%@/GetCodesForProject?projectid=%@&cataloguekind=%@&lang=%@", WEBSERVICE_ENDPOINT, map.projectid,  catalogue.type, langCode ] ];
    
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
	[request addValue: [ self getAuthorizationHeader:@"ronnie" password:@"123"]   forHTTPHeaderField:@"Authorization"];
    
    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:[ self retain ] startImmediately: NO ];
    
    self.conn = connection;
    [ connection release ];
    
    self.data = [[ NSMutableData alloc ] init ];
	
    [ self.conn start ];
	[ request release ];
    [ url release ];
}

-(void) finishedWithData:(NSData *)data
{
    // Load the xml answer
    NSXMLParser* parser = [[NSXMLParser alloc] initWithData:data];
    [parser setDelegate:(id)self];
    [parser setShouldResolveExternalEntities:YES];
    [ parser parse ];
    
    if ( finishedDelegate != nil && finishedAllSelector != nil )
        [ finishedDelegate performSelector: finishedAllSelector ];
	
}

#pragma mark - XMLParser delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
	
	//NSLog(@"element Name: %@, ", elementName);
    
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    NSEntityDescription *entity;
    
	
    if ( [elementName isEqualToString:@"Code"] ) 
    {
        entity = [ NSEntityDescription entityForName: @"Code" inManagedObjectContext: appDelegate.managedObjectContext ];
        
        self.currentCode = [[ Code alloc ] initWithEntity: entity insertIntoManagedObjectContext: appDelegate.managedObjectContext ];
        return;
    }
    else {
		currentSubPart = elementName;
	}
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    ADAYAppDelegate *appDelegate = [ [ UIApplication sharedApplication ] delegate ];
    
	if ( [elementName isEqualToString:@"Code"] ) {
        NSError *error;
        [ codes addObject: currentCode ];
        [ appDelegate.managedObjectContext save: &error ];
        
		if ( finishedDelegate != nil )
            [ finishedDelegate performSelector: finishedSelector withObject: currentCode ];
		currentSubPart = nil;
		return;
    }
    else {
		currentSubPart = nil;
	}
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    
    if ( [currentSubPart isEqualToString:@"Id"] ) {
        
        if ( currentCode.codeId == nil )
            currentCode.codeId = string;
        else
            currentCode.codeId  = [ NSString stringWithFormat: @"%@%@", currentCode.codeId, string ];
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"CodeCatalogId"] ) {
        
        if ( currentCode.catalogueid == nil )
            currentCode.catalogueid = string;
        else
            currentCode.catalogueid  = [ NSString stringWithFormat: @"%@%@", currentCode.catalogueid, string ];
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"Lang"]) {
        if ( currentCode.language == nil )
            currentCode.language = string;
        else
            currentCode.language  = [ NSString stringWithFormat: @"%@%@", currentCode.language, string ];
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"Text"]) {
        if ( currentCode.text == nil )
            currentCode.text = string;
        else
            currentCode.text  = [ NSString stringWithFormat: @"%@%@", currentCode.text, string ];
        return;
    }
    
    if ( [currentSubPart isEqualToString:@"Tags"]) {
        if ( currentCode.tags == nil )
            currentCode.tags = string;
        else
            currentCode.tags  = [ NSString stringWithFormat: @"%@%@", currentCode.tags, string ];
        return;
    }
    
}

- (NSDate*) parseRFC3339DateTimeString:(NSString *)rfc3339DateTimeString
// Returns a user-visible date time string that corresponds to the
// specified RFC 3339 date time string. Note that this does not handle
// all possible RFC 3339 date time strings, just one of the most common
// styles.
{
    NSString *          userVisibleDateTimeString;
    NSDateFormatter *   rfc3339DateFormatter;
    NSLocale *          enUSPOSIXLocale;
    
    userVisibleDateTimeString = nil;
    
    // Convert the RFC 3339 date time string to an NSDate.
    
    rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    
    enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    return [rfc3339DateFormatter dateFromString:rfc3339DateTimeString];
}

- (NSNumber*) parseStringToNumber:(NSString*)string {
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    return [f numberFromString:string];
}

@end
