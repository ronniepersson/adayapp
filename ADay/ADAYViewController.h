//
//  ADAYViewController.h
//  ADay
//
//  Created by Ronnie Persson on 2011-11-21.
//  Copyright (c) 2011 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADAYCatalogueWS.h"
#import "ADAYMapWS.h"
#import "ADAYCodeWS.h"
#import "SignInViewController.h"
#import "TextButton.h"
#import "Registration.h"
#import "ADAYInsertRegistrationsWS.h"

@interface ADAYViewController : UIViewController< UITableViewDataSource, UITableViewDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource, UIScrollViewDelegate>

@property ( nonatomic, retain ) NSArray *codes;
@property (nonatomic, retain ) NSArray *matchedWords;
@property ( nonatomic, retain ) ADAYInsertRegistrationsWS *registrations;
@property (retain, nonatomic) IBOutlet UITableView *words;
@property (retain, nonatomic) IBOutlet UITextView *input;
@property (retain, nonatomic) IBOutlet UIView *buttonView;
@property (retain, nonatomic) IBOutlet NSMutableArray *wordButtons;
@property ( retain, nonatomic ) Registration *reg;
@property BOOL pause;
@property BOOL changesTextByClicking;
@property (retain, nonatomic) IBOutlet UIButton *smileyButton;
- (IBAction)showSmileyPicker:(id)sender;
@property int smiley;
@property (retain, nonatomic) IBOutlet UIButton *submitButton;
@property (retain, nonatomic) IBOutlet UIPickerView *smileyPicker;
@property (retain, nonatomic) IBOutlet UIButton *whatButton;
@property (retain, nonatomic) IBOutlet UIButton *whereButton;
@property (retain, nonatomic) IBOutlet UIButton *whoButton;
- (IBAction)showWordsForTogether:(id)sender;
- (IBAction)showWordsForLocation:(id)sender;
- (IBAction)showWordsForActivity:(id)sender;
- (IBAction)clear:(id)sender;
- (IBAction)submit:(id)sender;
@end
