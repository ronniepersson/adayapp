//
//  Registration.h
//  Aday
//
//  Created by Stefan Norberg on 2011-11-07.
//  Copyright (c) 2011 Medc AB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Registration.h"

@interface Registration (Extended) 

- (NSString*) toXmlWithGuid: (NSString *) guid;
- (NSString*) dateToXmlString: ( NSDate *) date;
+(Registration *) insertRegistrationWithMap: ( Map *) map;

@end
