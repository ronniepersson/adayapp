//
//  InfoViewController.m
//  ADay
//
//  Created by Ronnie Persson on 2012-04-20.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController
@synthesize infoWebView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString( @"Help", @"Help dialog title" );
    
    [ self.infoWebView loadRequest:  [ NSURLRequest requestWithURL: [ NSURL URLWithString: NSLocalizedString(@"helpwww", @"Adress to helpsite") ] ] ];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setInfoWebView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [infoWebView release];
    [super dealloc];
}

@end
